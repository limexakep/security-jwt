CREATE TABLE user_role
(
    id   serial8      NOT NULL,
    user_id BIGINT NOT NULL,
    role_id BIGINT NOT NULL,
    CONSTRAINT pk_user_role PRIMARY KEY (id),
    CONSTRAINT fk_user_role_on_user FOREIGN KEY (user_id) REFERENCES "user" (id),
    CONSTRAINT fk_user_role_on_role FOREIGN KEY (role_id) REFERENCES role (id)
)