CREATE TABLE "user"
(
    id       serial8      NOT NULL,
    username VARCHAR(100) NOT NULL,
    password VARCHAR(100) NOT NULL,
    enabled  BOOLEAN      NOT NULL,
    CONSTRAINT pk_user PRIMARY KEY (id)
)