CREATE TABLE role
(
    id   serial8      NOT NULL,
    name VARCHAR(100) NOT NULL,
    CONSTRAINT pk_role PRIMARY KEY (id)
)