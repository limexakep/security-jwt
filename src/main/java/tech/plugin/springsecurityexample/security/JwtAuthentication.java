package tech.plugin.springsecurityexample.security;

import java.util.*;

import lombok.*;
import org.springframework.security.core.*;
import tech.plugin.springsecurityexample.entity.*;

@AllArgsConstructor
public class JwtAuthentication implements Authentication {
    private String username;
    private boolean authenticated;
    private Set<Role> roles;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles;
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getDetails() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return username;
    }

    @Override
    public boolean isAuthenticated() {
        return authenticated;
    }

    @Override
    public void setAuthenticated(boolean authenticated) throws IllegalArgumentException {
        this.authenticated = authenticated;
    }

    @Override
    public String getName() {
        return this.username;
    }

}
