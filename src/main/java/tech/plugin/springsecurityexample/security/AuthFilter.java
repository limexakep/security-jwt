package tech.plugin.springsecurityexample.security;

import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;

import lombok.*;
import org.springframework.security.core.context.*;
import org.springframework.stereotype.*;
import org.springframework.util.*;
import org.springframework.web.filter.*;
import tech.plugin.springsecurityexample.exception.*;

@Component
@RequiredArgsConstructor
public class AuthFilter extends OncePerRequestFilter {
    private final JwtUtils jwtUtils;
    private static final String TOKEN_NAME = "Bearer ";

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
        throws ServletException, IOException {
        String authHeader = request.getHeader("Authorization");
        if (StringUtils.hasText(authHeader) && authHeader.startsWith(TOKEN_NAME)) {
            String token = authHeader.substring(TOKEN_NAME.length());

            if (!jwtUtils.validateToken(token)) {
                throw new JwtException("Invalid token");
            }

            String username = jwtUtils.getUserNameFromToken(token);

            if (username == null) {
                throw new JwtException("Token doesn't contain username claim");
            }

            JwtAuthentication authentication = jwtUtils.generateAuth(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }

        filterChain.doFilter(request, response);
    }

}
