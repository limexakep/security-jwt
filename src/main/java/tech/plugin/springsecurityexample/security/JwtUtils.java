package tech.plugin.springsecurityexample.security;

import java.util.*;
import java.util.stream.*;

import com.auth0.jwt.*;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.*;
import com.auth0.jwt.exceptions.*;
import com.auth0.jwt.interfaces.*;
import lombok.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.*;
import org.springframework.stereotype.*;
import tech.plugin.springsecurityexample.entity.*;
import tech.plugin.springsecurityexample.exception.*;
import tech.plugin.springsecurityexample.service.*;

@Component
@RequiredArgsConstructor
public class JwtUtils {
    @Value("${tech.plugin.jwt.secret-key}")
    private String secretKey;

    @Value("${tech.plugin.jwt.exp}")
    private Long expirationTime;

    @Value("${tech.plugin.jwt.issuer}")
    private String issuer;

    private final UserService userService;

    public boolean validateToken(String token) {
        DecodedJWT decodedJWT = getDecodedJWT(token);

        return decodedJWT != null;
    }

    public JwtAuthentication generateAuth(String token) {
        DecodedJWT decodedJWT = getDecodedJWT(token);

        if (decodedJWT == null) {
            throw new JwtException("Invalid token");
        }

        String subject = decodedJWT.getSubject();
        Claim roleClaim = decodedJWT.getClaim("roles");

        if (roleClaim.isMissing() || roleClaim.isNull()) {
            throw new JwtException("Missing user role claim");
        }

        List<String> roleList = roleClaim.asList(String.class);

        Set<Role> roles = roleList.stream()
            .map(Role::new)
            .collect(Collectors.toSet());

        return new JwtAuthentication(subject, true, roles);
    }

    private DecodedJWT getDecodedJWT(String token) {
        Algorithm algorithm = Algorithm.HMAC256(secretKey);

        JWTVerifier verifier = JWT.require(algorithm)
            .withIssuer(issuer)
            .build();

        try {
            return verifier.verify(token);
        } catch (JWTVerificationException e) {
            return null;
        }
    }

    public String generateToken(Authentication authentication) {
        User userPrincipal = (User) userService.loadUserByUsername(authentication.getPrincipal().toString());

        Algorithm algorithm = Algorithm.HMAC256(secretKey);

        List<String> roles = userPrincipal.getRoles().stream()
            .map(Role::getName)
            .collect(Collectors.toList());

        String jwtToken = JWT.create()
            .withIssuer(issuer)
            .withSubject(userPrincipal.getUsername())
            .withClaim("roles", roles)
            .withIssuedAt(new Date())
            .withNotBefore(new Date())
            .withExpiresAt(new Date(System.currentTimeMillis() + expirationTime))
            .withJWTId(UUID.randomUUID().toString())
            .sign(algorithm);

        return jwtToken;
    }

    public String getUserNameFromToken(String token) {
        DecodedJWT decodedJWT = getDecodedJWT(token);

        if (decodedJWT != null) {
            return decodedJWT.getSubject();
        } else {
            return null;
        }
    }
}
