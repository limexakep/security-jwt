package tech.plugin.springsecurityexample.exception;

import org.springframework.http.*;

public class JwtException extends ApiException {
    public JwtException(String message) {
        super(HttpStatus.UNAUTHORIZED, message);
    }
}
