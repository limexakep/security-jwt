package tech.plugin.springsecurityexample.exception;

import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import tech.plugin.springsecurityexample.dto.*;

@ControllerAdvice
public class ApiExceptionHandler {
    @ExceptionHandler(ApiException.class)
    public ResponseEntity<ApiErrorDto> handleApiException(ApiException apiException) {
        ApiErrorDto apiError = new ApiErrorDto(
            apiException.getMessage(),
            apiException.getTimestamp()
        );

        return ResponseEntity.status(apiException.getHttpStatus()).body(apiError);
    }
}
