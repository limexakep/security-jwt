package tech.plugin.springsecurityexample.exception;

import org.springframework.http.*;

public class WrongArgumentException extends ApiException {

    public WrongArgumentException(String message) {
        super(HttpStatus.BAD_REQUEST, message);
    }

}
