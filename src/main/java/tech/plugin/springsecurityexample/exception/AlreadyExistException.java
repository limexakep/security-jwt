package tech.plugin.springsecurityexample.exception;

import org.springframework.http.*;

public class AlreadyExistException extends ApiException {

    public AlreadyExistException(String message) {
        super(HttpStatus.CONFLICT, message);
    }

}
