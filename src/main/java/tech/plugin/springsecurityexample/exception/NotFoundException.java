package tech.plugin.springsecurityexample.exception;

import org.springframework.http.*;

public class NotFoundException extends ApiException {

    public NotFoundException(String message) {
        super(HttpStatus.NOT_FOUND, message);
    }

}
