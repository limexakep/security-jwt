package tech.plugin.springsecurityexample.dto;

import lombok.*;

@Value
public class TokenResponseDto {
    String accessToken;
}
