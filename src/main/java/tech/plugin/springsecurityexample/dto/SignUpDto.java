package tech.plugin.springsecurityexample.dto;

import lombok.*;

@Value
public class SignUpDto {
    String username;

    String password;

    String confirmPassword;

}
