package tech.plugin.springsecurityexample.dto;

import java.time.*;

import lombok.*;

@Value
public class ApiErrorDto {
    String message;

    ZonedDateTime timestamp;
}
