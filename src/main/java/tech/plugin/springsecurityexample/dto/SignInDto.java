package tech.plugin.springsecurityexample.dto;

import lombok.*;

@Value
public class SignInDto {
    String username;

    String password;
}
