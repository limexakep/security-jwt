package tech.plugin.springsecurityexample.repository;

import java.util.*;

import org.springframework.data.repository.*;
import org.springframework.stereotype.*;
import org.springframework.stereotype.Repository;
import tech.plugin.springsecurityexample.entity.*;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    User findByUsername(String username);
}
