package tech.plugin.springsecurityexample.controller;

import java.util.*;

import lombok.*;
import org.springframework.http.*;
import org.springframework.security.core.*;
import org.springframework.security.crypto.password.*;
import org.springframework.web.bind.annotation.*;
import tech.plugin.springsecurityexample.dto.*;
import tech.plugin.springsecurityexample.entity.*;
import tech.plugin.springsecurityexample.exception.*;
import tech.plugin.springsecurityexample.security.*;
import tech.plugin.springsecurityexample.service.*;

@RestController
@RequestMapping("/api/auth")
@RequiredArgsConstructor
public class AuthController {

    private final UserService userService;

    private final JwtUtils jwtUtils;

    private final PasswordEncoder passwordEncoder;

    @PostMapping("/login")
    ResponseEntity<TokenResponseDto> signIn(@RequestBody SignInDto signInDto) {
        User user = (User) userService.loadUserByUsername(signInDto.getUsername());

        if (user == null) {
            throw new NotFoundException("User not found");
        }

        if (!passwordEncoder.matches(signInDto.getPassword(), user.getPassword())) {
            throw new NotFoundException("Password doesn't match");
        }

        Authentication authentication = new JwtAuthentication(
            user.getUsername(), true, user.getRoles()
        );

        return ResponseEntity.ok(
            new TokenResponseDto(jwtUtils.generateToken(authentication))
        );
    }

    @PostMapping("/create")
    ResponseEntity<String> signUp(@RequestBody SignUpDto signUpDto) {
        User user = (User) userService.loadUserByUsername(signUpDto.getUsername());

        if (user != null) {
            throw new AlreadyExistException("User with this username already exist");
        }

        if (!signUpDto.getPassword().equals(signUpDto.getConfirmPassword())) {
            throw new WrongArgumentException("Passwords doesn't match");
        }

        Set<Role> roles = Collections.singleton(new Role("ROLE_USER"));

        user = new User(
            signUpDto.getUsername(),
            passwordEncoder.encode(signUpDto.getPassword()),
            roles
        );

        userService.createUser(user);

        return ResponseEntity.status(HttpStatus.CREATED).body("User successfully created");
    }

}
