package tech.plugin.springsecurityexample.controller;

import javax.servlet.http.*;
import org.springframework.http.*;
import org.springframework.security.access.prepost.*;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class HelloController {
    @GetMapping("/all")
    public ResponseEntity<String> sayHelloAll() {
        return ResponseEntity.ok("Hello all!");
    }

    @GetMapping("/user")
    @PreAuthorize("hasAuthority('ROLE_USER') or hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<String> sayHelloUser(HttpServletRequest request) {
        return ResponseEntity.ok("Hello user " + request.getUserPrincipal().getName());
    }
    @GetMapping("/admin")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<String> sayHelloAdmin(HttpServletRequest request) {
        return ResponseEntity.ok("Hello admin "  + request.getUserPrincipal().getName());
    }
}
